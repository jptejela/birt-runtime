# README #

Ficheros modificados del jar original org.eclipse.birt.runtime-4.6.0-20160607a.jar.

Este jar se mantiene en el repositorio maven local que tenemos en 'blogportal/maven-local-repo'

Para generar un nuevo jar hacemos unzip/zip y sustituimos los .class obtenidos con la compilación de este repo.

El script build/build.sh hace estas operaciones.

Editar las variables con el nombre del jar a fuente t destino y ejecutar:

    ./build/build.sh
    
El jar resultante se copia a una carpeta en el repositorio 'blogportal/maven-local-repo'

Copiar manualmente el fichero pom de una version anterior, renombrarlo, y editar la versión dentro del pom

