#!/bin/bash
set -ex

ROOT_DIR=$PWD

SRC_DIR=../blogportal/maven-local-repo/org/eclipse/birt/runtime/org.eclipse.birt.runtime
SRC_VERSION=4.6.0-20160607a
SRC_FILE=org.eclipse.birt.runtime-$SRC_VERSION.jar
SRC=$SRC_DIR/$SRC_VERSION/$SRC_FILE

TGT_DIR=./target
TGT_VERSION=4.6.0-20160607c
TGT_FILE=org.eclipse.birt.runtime-$TGT_VERSION.jar
TGT=$TGT_DIR/$TGT_FILE

CLASS_DIR=./bin

PACKAGE_DIR=$TGT_DIR/package

rm -fr $TGT_DIR
mkdir -p $PACKAGE_DIR

(cd $PACKAGE_DIR && jar xvf $ROOT_DIR/$SRC)
cp -Rp $CLASS_DIR/* $PACKAGE_DIR
jar Mcvf $TGT -C $PACKAGE_DIR .

mkdir -p $SRC_DIR/$TGT_VERSION
cp $TGT $SRC_DIR/$TGT_VERSION
